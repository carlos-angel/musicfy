import { BrowserRouter } from "react-router-dom";
import { useAuth } from "hooks/useAuth.hook";
import { AuthRoutes } from "./AuthRoutes";
import { DashboardRoutes } from "./DashboardRoutes";
import { PlayerProvider } from "context/player";

export const AppRoutes = () => {
  const { isAuthenticated } = useAuth();

  if (!isAuthenticated) {
    return (
      <BrowserRouter>
        <AuthRoutes />
      </BrowserRouter>
    );
  }

  return (
    <PlayerProvider>
      <BrowserRouter>
        <DashboardRoutes />
      </BrowserRouter>
    </PlayerProvider>
  );
};
