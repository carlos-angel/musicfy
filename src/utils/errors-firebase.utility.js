import { toast } from "react-toastify";

const codes = {
  'auth/user-not-found': 'El usuario no existe',
  'auth/wrong-password': 'La contraseña es incorrecta',
  'auth/email-already-in-use': 'El email ya está en uso',
  'auth/invalid-email': 'El email es inválido',
  'auth/too-many-requests': 'Demasiados intentos fallidos',
  'auth/unknown': 'Error desconocido',
}

export const handleErrorFirebase = (code) => {
  const message = codes[code] || codes['auth/unknown'];
  if (message) {
    toast.error(message);
  }
};