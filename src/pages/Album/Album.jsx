import { Loader } from "semantic-ui-react";
import { Link, useParams } from "react-router-dom";
import { SongsList } from "components/Songs/List";
import { useAlbum } from "hooks/useAlbum.hook";
import { useImage } from "hooks/useImage.hook";
import { useArtist } from "hooks/useArtist.hook";
import { useSongsFromAlbum } from "hooks/useSongsFromAlbum.hook";
import "./styles.scss";

export const Album = () => {
  const { id } = useParams();
  const album = useAlbum({ id });
  const albumImage = useImage({ folder: "album", imageName: album?.banner });
  const artist = useArtist({ id: album?.artist });
  const songs = useSongsFromAlbum({ albumId: id });

  return !album || !artist ? (
    <Loader active>Cargando</Loader>
  ) : (
    <div className="album">
      <HeaderAlbum album={album} artist={artist} albumImage={albumImage} />
      <div className="album__songs">
        <SongsList songs={songs} albumImage={albumImage} />
      </div>
    </div>
  );
};

function HeaderAlbum({ album, artist, albumImage }) {
  return (
    <div className="album__header">
      <div
        className="image"
        style={{ backgroundImage: `url('${albumImage}')` }}
      />
      <div className="info">
        <h1>{album.name}</h1>
        <p>
          De{" "}
          <span>
            <Link to={`/artist/${artist.id}`}>{artist.name}</Link>
          </span>
        </p>
      </div>
    </div>
  );
}
