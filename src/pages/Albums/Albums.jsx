import { Grid } from "semantic-ui-react";
import { map } from "lodash";
import { Link } from "react-router-dom";
import { useAlbums } from "hooks/useAlbums.hook";
import { useImage } from "hooks/useImage.hook";
import "./styles.scss";

export const Albums = () => {
  const albums = useAlbums();
  return (
    <div className="albums">
      <h1>Álbumes</h1>
      <Grid>
        {map(albums, (album) => (
          <Grid.Column key={album.id} mobile={8} tablet={4} computer={3}>
            <Album {...album} />
          </Grid.Column>
        ))}
      </Grid>
    </div>
  );
};

function Album({ name, banner, id }) {
  const bannerUrl = useImage({ folder: "album", imageName: banner });
  return (
    <Link to={`album/${id}`}>
      <div className="albums__item">
        <div
          className="avatar"
          style={{ backgroundImage: `url('${bannerUrl}')` }}
        />
        <h3>{name}</h3>
      </div>
    </Link>
  );
}
