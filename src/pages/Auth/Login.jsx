import { LoginForm } from 'components/Auth/LoginForm';

import BackgroundAuth from 'assets/jpg/background-auth.jpg';
import LogoNameWithe from 'assets/png/logo-name-white.png';
import './styles.scss';import React from 'react'

export default function Login() {
    return (
        <div
          className='auth'
          style={{ backgroundImage: `url('${BackgroundAuth}')` }}
        >
          <div className='auth__dark' />
    
          <div className='auth__box'>
            <div className='auth__box-logo'>
              <img src={LogoNameWithe} alt='Musicfy' />
            </div>
            <LoginForm />
          </div>
        </div>
      );
}
