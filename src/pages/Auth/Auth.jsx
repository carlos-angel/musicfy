import { Link } from "react-router-dom";
import BackgroundAuth from "assets/jpg/background-auth.jpg";
import LogoNameWithe from "assets/png/logo-name-white.png";
import "./styles.scss";

export const Auth = () => {
  return (
    <div
      className="auth"
      style={{ backgroundImage: `url('${BackgroundAuth}')` }}
    >
      <div className="auth__dark" />

      <div className="auth__box">
        <div className="auth__box-logo">
          <img src={LogoNameWithe} alt="Musicfy" />
        </div>
        <div className="auth-options">
          <h2>Millones de canciones, gratis en Musicfy</h2>
          <Link className="register" to="/auth/register">
            Registrate gratis
          </Link>
          <Link className="login" to="/auth/login">
            Iniciar sesión
          </Link>
        </div>
      </div>
    </div>
  );
};
