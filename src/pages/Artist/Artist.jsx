import { useParams } from "react-router-dom";
import { BannerArtist } from "components/Artists/BannerArtist";
import { BasicSliderItems } from "components/Sliders/BasicSliderItems";
import { SongsSlider } from "components/Sliders/SongsSlider";
import { useArtist } from "hooks/useArtist.hook";
import { useAlbumsFromArtist } from "hooks/useAlbumsFromArtist.hook";
import { useSongsFromAlbums } from "hooks/useSongsFromAlbums.hook";
import "./styles.scss";

export const Artist = () => {
  const { id } = useParams();
  const artist = useArtist({ id });
  const albums = useAlbumsFromArtist({ artistId: id });
  const songs = useSongsFromAlbums(albums);

  return (
    <div className="artist">
      {artist && <BannerArtist artist={artist} />}
      <div className="artist__content">
        <BasicSliderItems
          title="Álbumes"
          data={albums}
          folderImages="album"
          urlName="album"
        />

        <SongsSlider title="Canciones" data={songs} />
      </div>
    </div>
  );
};
