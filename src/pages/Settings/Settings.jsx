import { UploadAvatar } from "components/Settings/UploadAvatar";
import { UserName } from "components/Settings/UserName";
import { BasicModal } from "components/Modal/Basic";
import { useModal } from "hooks/useModal";
import { NameForm } from "components/Settings/NameForm";
import { UserEmail } from "components/Settings/UserEmail";
import { EmailForm } from "components/Settings/EmailForm";
import { UserPassword } from "components/Settings/UserPassword";
import { PasswordForm } from "components/Settings/PasswordForm";
import { useAuth } from "hooks/useAuth.hook";
import "./styles.scss";

export const Settings = () => {
  const { user, onReload } = useAuth();
  const {
    properties: { show, title, content },
    setShow,
    setTitle,
    setContent,
  } = useModal();

  const handleEdit = () => {
    setShow(true);
    setTitle("Actualizar Nombre");
    setContent(<NameForm setShowModal={setShow} />);
  };

  const handleEditEmail = () => {
    setShow(true);
    setTitle("Actualizar Email");
    setContent(<EmailForm setShowModal={setShow} />);
  };

  const handleEditPassword = () => {
    setShow(true);
    setTitle("Actualizar Password");
    setContent(<PasswordForm setShowModal={setShow} />);
  };

  return (
    <div className='settings'>
      <h1>Configuración</h1>
      <div className='avatar-name'>
        <UploadAvatar user={user} onReload={onReload} />
        <UserName user={user} handleEdit={handleEdit} />
      </div>
      <UserEmail user={user} handleEdit={handleEditEmail} />
      <UserPassword user={user} handleEdit={handleEditPassword} />
      <BasicModal show={show} title={title} setShow={setShow}>
        {content}
      </BasicModal>
    </div>
  );
};
