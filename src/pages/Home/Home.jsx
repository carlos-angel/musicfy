import { BannerHome } from "components/BannerHome";
import { BasicSliderItems } from "components/Sliders/BasicSliderItems";
import { SongsSlider } from "components/Sliders/SongsSlider";
import { useArtists } from "hooks/useArtists.hook";
import { useAlbums } from "hooks/useAlbums.hook";
import { useSongs } from "hooks/useSongs";
import "./styles.scss";

export const Home = () => {
  const artists = useArtists();
  const albums = useAlbums();
  const songs = useSongs();

  return (
    <>
      <BannerHome />
      <div className="home">
        <BasicSliderItems
          title="Últimos artistas"
          data={artists}
          folderImages="artist"
          urlName="artist"
        />
        <BasicSliderItems
          title="Últimos álbumes"
          data={albums}
          folderImages="album"
          urlName="album"
        />
        <SongsSlider title="Ultimas canciones" data={songs} />
      </div>
    </>
  );
};
