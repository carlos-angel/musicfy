export const INITIAL_STATE = {
  isAuthenticated: false,
  isAdmin: false,
  user: null,
  error: false,
  loading: false,
};

const types = {
  AUTHENTICATE_REQUEST: "AUTHENTICATE_REQUEST",
  AUTHENTICATE_SUCCESS: "AUTHENTICATE_SUCCESS",
  AUTHENTICATE_FAILURE: "AUTHENTICATE_FAILURE",
  LOGOUT: "LOGOUT",
};

export function authReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.AUTHENTICATE_REQUEST:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case types.AUTHENTICATE_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload.user,
        isAdmin: action.payload.isAdmin,
        loading: false,
        error: false,
      };
    case types.AUTHENTICATE_FAILURE:
      return {
        ...state,
        isAuthenticated: false,
        isAdmin: false,
        user: null,
        loading: false,
        error: true,
      };
    case types.LOGOUT:
      return {
        ...state,
        isAuthenticated: false,
        user: null,
        isAdmin: false,
        loading: false,
        error: false,
      };
    default:
      return state;
  }
}

export const actions = {
  authenticateRequest: () => ({
    type: types.AUTHENTICATE_REQUEST,
  }),
  authenticateSuccess: (payload) => ({
    type: types.AUTHENTICATE_SUCCESS,
    payload,
  }),
  authenticateFailure: () => ({
    type: types.AUTHENTICATE_FAILURE,
  }),
  logout: () => ({
    type: types.LOGOUT,
  }),
};
