import { useState, useEffect } from "react";
import { getImage } from "services/storage";

export function usePlayerProvider() {
  const [song, setSong] = useState(null);
  const [playing, setPlaying] = useState(false);
  const [playerSeconds, setPlayerSeconds] = useState(0);
  const [totalSeconds, setTotalSeconds] = useState(0);
  const [volume, setVolume] = useState(0.3);

  useEffect(() => {
    if (song?.url) {
      onStart();
    }
  }, [song]);

  const onStart = () => {
    setPlaying(true);
  };

  const onStop = () => {
    setPlaying(false);
  };

  const onProgress = (data) => {
    const { playedSeconds, loadedSeconds } = data;
    setPlayerSeconds(playedSeconds);
    setTotalSeconds(loadedSeconds);
  };

  const player = async ({ albumImage, songName, songNameFile }) => {
    const url = await getImage({ folder: "songs", imageName: songNameFile });
    setSong({ image: albumImage, name: songName, url });
  };

  const onChangeVolume = (value) => {
    setVolume(Number(value));
  };

  return {
    song,
    playing,
    playerSeconds,
    totalSeconds,
    volume,
    player,
    onStart,
    onStop,
    onProgress,
    onChangeVolume,
  };
}
