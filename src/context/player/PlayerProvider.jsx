import { createContext, useMemo } from "react";
import { usePlayerProvider } from "./usePlayerProvider.hook";

export const PlayerContext = createContext({
  song: null,
  playing: false,
  playerSeconds: 0,
  totalSeconds: 0,
  volume: 0,
  player: async ({ albumImage, songName, songNameFile }) => {},
  onStart: () => {},
  onStop: () => {},
  onProgress: (data) => {},
  onChangeVolume: (value) => {},
});

export function PlayerProvider({ children }) {
  const state = usePlayerProvider();
  const player = useMemo(() => state, [state]);

  return (
    <PlayerContext.Provider value={player}>{children}</PlayerContext.Provider>
  );
}
