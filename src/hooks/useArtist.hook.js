import { useState, useEffect } from "react";
import { getArtist } from "services/artists";

export function useArtist({ id }) {
  const [artist, setArtist] = useState(null);
  useEffect(() => {
    if (id) {
      getArtist(id)
        .then(setArtist)
        .catch(() => setArtist(null));
    }
  }, [id]);

  return artist;
}
