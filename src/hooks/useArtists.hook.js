import { useState, useEffect } from "react";
import { getArtists } from "services/artists";

export function useArtists() {
  const [artists, setArtists] = useState([]);

  useEffect(() => {
    getArtists()
      .then((response) => {
        setArtists(response);
      })
      .catch(() => setArtists([]));
  }, []);

  return artists;
}
