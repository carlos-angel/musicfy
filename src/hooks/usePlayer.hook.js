import { useContext } from "react";
import { PlayerContext } from "context/player";

export function usePlayer() {
  return useContext(PlayerContext);
}
