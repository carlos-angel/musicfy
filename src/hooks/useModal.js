import { useState } from 'react';

export const useModal = () => {
  const [title, setTitle] = useState('');
  const [show, setShow] = useState(false);
  const [content, setContent] = useState(null);
  return {
    properties: {
      title,
      show,
      content,
    },
    setShow,
    setTitle,
    setContent,
  };
};
