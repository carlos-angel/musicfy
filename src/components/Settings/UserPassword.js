import React from 'react';
import { Button } from 'semantic-ui-react';

export const UserPassword = ({ handleEdit }) => {
  return (
    <div className='user-password'>
      <h3>Contraseña: *** *** ***</h3>
      <Button circular onClick={handleEdit}>
        Actualizar
      </Button>
    </div>
  );
};
