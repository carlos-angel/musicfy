import { Button, FormField } from "semantic-ui-react";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import { Input } from "components/common/Form";
import { toast } from "react-toastify";
import { useAuth } from "hooks/useAuth.hook";
import { updateUsername } from "services/user";

export const NameForm = ({ setShowModal }) => {
  const { user } = useAuth();

  return (
    <Formik
      initialValues={{ displayName: user.displayName }}
      validationSchema={Yup.object({
        displayName: Yup.string().required("El nombre es requerido"),
      })}
      onSubmit={async ({ displayName }) => {
        try {
          await updateUsername({ userFirebase: user, username: displayName });
          toast.success("Nombre actualizado");
          setShowModal(false);
        } catch (error) {
          toast.error("Error al actualizar el nombre");
        }
      }}
    >
      {({ isSubmitting }) => (
        <Form>
          <FormField>
            <Input
              type="text"
              name="displayName"
              placeholder="Nombre del usuario"
            />
          </FormField>
          <Button loading={isSubmitting} type="submit" disabled={isSubmitting}>
            Actualizar nombre
          </Button>
        </Form>
      )}
    </Formik>
  );
};
