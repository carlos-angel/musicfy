import { Icon, Button, FormField } from "semantic-ui-react";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import { Input } from "components/common/Form";
import { usePassword } from "hooks/usePassword";
import { updatePassword } from "services/user";

export const PasswordForm = ({ setShowModal }) => {
  const [showPassword, handleShowPassword] = usePassword();
  const [showNewPassword, handleShowNewPassword] = usePassword();
  return (
    <Formik
      initialValues={{ oldPassword: "", newPassword: "", confirmPassword: "" }}
      validationSchema={Yup.object({
        oldPassword: Yup.string().required("La contraseña actual es requerida"),
        newPassword: Yup.string().required("La nueva contraseña es requerida"),
        confirmPassword: Yup.string()
          .oneOf([Yup.ref("newPassword"), null], "Las contraseñas no coinciden")
          .required("confirme la nueva contraseña"),
      })}
      onSubmit={async ({ oldPassword, newPassword }) => {
        const isUpdated = await updatePassword({ oldPassword, newPassword });
        if (isUpdated) {
          setShowModal(false);
        }
      }}
    >
      {({ isSubmitting }) => (
        <Form>
          <FormField>
            <Input
              name="oldPassword"
              type={showPassword ? "text" : "password"}
              placeholder="Contraseña"
              icon={
                showPassword ? (
                  <Icon
                    name="eye slash outline"
                    link
                    onClick={handleShowPassword}
                  />
                ) : (
                  <Icon name="eye" link onClick={handleShowPassword} />
                )
              }
            />
          </FormField>
          <FormField>
            <Input
              name="newPassword"
              type={showNewPassword ? "text" : "password"}
              placeholder="Contraseña"
              icon={
                showNewPassword ? (
                  <Icon
                    name="eye slash outline"
                    link
                    onClick={handleShowNewPassword}
                  />
                ) : (
                  <Icon name="eye" link onClick={handleShowNewPassword} />
                )
              }
            />
          </FormField>
          <FormField>
            <Input
              name="confirmPassword"
              type={showNewPassword ? "text" : "password"}
              placeholder="Contraseña"
              icon={
                showNewPassword ? (
                  <Icon
                    name="eye slash outline"
                    link
                    onClick={handleShowNewPassword}
                  />
                ) : (
                  <Icon name="eye" link onClick={handleShowNewPassword} />
                )
              }
            />
          </FormField>
          <Button type="submit" loading={isSubmitting} disabled={isSubmitting}>
            Actualizar contraseña
          </Button>
        </Form>
      )}
    </Formik>
  );
};
