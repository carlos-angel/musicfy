import { useState, useCallback } from "react";
import { Image } from "semantic-ui-react";
import { useDropzone } from "react-dropzone";
import NotAvatar from "assets/png/user.png";
import { uploadAvatar } from "services/user";

export const UploadAvatar = ({ user, onReload }) => {
  const [avatarUrl, setAvatarUrl] = useState(user.photoURL);

  const onDrop = useCallback(
    async (acceptedFiles) => {
      const file = acceptedFiles[0];
      setAvatarUrl(URL.createObjectURL(file));
      await uploadAvatar(file);
      await onReload();
    },
    [onReload]
  );

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    accept: "image/jpeg, image/png, image/jpg",
    noKeyboard: true,
    onDrop,
  });

  return (
    <div className='user-avatar' {...getRootProps()}>
      <input {...getInputProps()} />
      {isDragActive ? (
        <Image src={NotAvatar} />
      ) : (
        <Image src={avatarUrl || NotAvatar} />
      )}
    </div>
  );
};
