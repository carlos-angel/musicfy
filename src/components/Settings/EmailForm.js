import { Icon, Button, FormField } from "semantic-ui-react";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import { Input } from "components/common/Form";
import { usePassword } from "hooks/usePassword";
import { useAuth } from "hooks/useAuth.hook";
import { updateEmail } from "services/user";

export const EmailForm = ({ setShowModal }) => {
  const { user } = useAuth();
  const [showPassword, handleShowPassword] = usePassword();

  return (
    <Formik
      initialValues={{ email: user.email, password: "" }}
      validationSchema={Yup.object({
        email: Yup.string()
          .email("El email no es válido")
          .required("El email es requerido"),
        password: Yup.string().required("La contraseña es requerida"),
      })}
      onSubmit={async ({ email, password }) => {
        const isUpdated = await updateEmail({ email, password });
        if (isUpdated) {
          setShowModal(false);
        }
      }}
    >
      {({ isSubmitting }) => (
        <Form>
          <FormField>
            <Input name="email" type="email" />
          </FormField>
          <FormField>
            <Input
              name="password"
              type={showPassword ? "text" : "password"}
              placeholder="Contraseña"
              icon={
                showPassword ? (
                  <Icon
                    name="eye slash outline"
                    link
                    onClick={handleShowPassword}
                  />
                ) : (
                  <Icon name="eye" link onClick={handleShowPassword} />
                )
              }
            />
          </FormField>
          <Button type="submit" loading={isSubmitting} disabled={isSubmitting}>
            Actualizar Email
          </Button>
        </Form>
      )}
    </Formik>
  );
};
