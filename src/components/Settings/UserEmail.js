import React from 'react';
import { Button } from 'semantic-ui-react';

export const UserEmail = ({ user, handleEdit }) => {
  return (
    <div className='user-email'>
      <h3>Email: {user.email}</h3>
      <Button circular onClick={() => handleEdit(user)}>
        Actualizar
      </Button>
    </div>
  );
};
