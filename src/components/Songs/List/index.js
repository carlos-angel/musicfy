import { map } from "lodash";
import { Table, Icon } from "semantic-ui-react";
import { usePlayer } from "hooks/usePlayer.hook";
import "./styles.scss";

export const SongsList = ({ songs, albumImage }) => {
  const { player } = usePlayer();
  return (
    <Table inverted className="list-songs">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell />
          <Table.HeaderCell> Titulo</Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        {map(songs, (song) => (
          <Song
            key={song.id}
            {...song}
            player={player}
            albumImage={albumImage}
          />
        ))}
      </Table.Body>
    </Table>
  );
};

function Song(props) {
  const { name, fileName, player, albumImage } = props;
  const onPlay = async () => {
    await player({ albumImage, songName: name, songNameFile: fileName });
  };

  return (
    <Table.Row onClick={onPlay}>
      <Table.Cell collapsing>
        <Icon name="play circle outline" />
      </Table.Cell>
      <Table.Cell>{name}</Table.Cell>
    </Table.Row>
  );
}
