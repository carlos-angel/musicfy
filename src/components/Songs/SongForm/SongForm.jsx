import { useState, useEffect, useCallback } from "react";
import { map } from "lodash";
import { Button, Icon, FormField } from "semantic-ui-react";
import { useDropzone } from "react-dropzone";
import { Formik, Form } from "formik";
import { Input, Dropdown } from "components/common/Form";
import * as Yup from "yup";
import { useAlbums } from "hooks/useAlbums.hook";
import { toast } from "react-toastify";
import { addSong } from "services/songs";
import "./styles.scss";

export const SongForm = ({ setShowModal }) => {
  const albums = useAlbums();
  const [albumsDropdownOptions, setAlbumsDropdownOptions] = useState([]);
  const [file, setFile] = useState(null);

  useEffect(() => {
    if (albums) {
      const data = map(albums, (album) => ({
        key: album.id,
        text: album.name,
        value: album.id,
      }));
      setAlbumsDropdownOptions(data);
    }
  }, [albums]);

  const onDrop = useCallback((acceptedFiles) => {
    const file = acceptedFiles[0];
    setFile(file);
  }, []);

  const { getRootProps, getInputProps } = useDropzone({
    accept: ".mp3",
    noKeyboard: true,
    onDrop,
  });

  return (
    <Formik
      initialValues={{ album: "", name: "" }}
      validationSchema={Yup.object({
        name: Yup.string().required(
          "El nombre de la canción no puede estar vacío"
        ),
        album: Yup.string().required("Seleccione un álbum de la lista"),
      })}
      onSubmit={async (values) => {
        if (!file) {
          toast.error("Seleccione un archivo de audio");
          return;
        }

        await addSong({ ...values, song: file });
        setShowModal(false);
      }}
    >
      {({ isSubmitting, values, setFieldValue, handleBlur }) => (
        <Form className="song-form">
          <Input placeholder="Nombre de la canción" name="name" />

          <Dropdown
            placeholder="asignar la canción a un álbum"
            name="album"
            search
            selection
            lazyLoad
            options={albumsDropdownOptions}
            value={values.album}
            onChange={(e, { value }) => setFieldValue("album", value)}
            onBlur={() => handleBlur("album")}
          />

          <FormField>
            <div className="song-upload" {...getRootProps()}>
              <input {...getInputProps()} />
              <Icon name="cloud upload" className={file && "load"} />
              <div>
                <p>
                  Arrastra tu canción o haz click <span>aquí</span>
                </p>
                {file && (
                  <p>
                    Canción subida: <span>{file.name}</span>
                  </p>
                )}
              </div>
            </div>
          </FormField>
          <Button loading={isSubmitting} disabled={isSubmitting} type="submit">
            Subir canción
          </Button>
        </Form>
      )}
    </Formik>
  );
};
