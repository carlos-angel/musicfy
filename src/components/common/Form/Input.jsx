import { FormField, Input as InputSUR } from 'semantic-ui-react';
import {useField, ErrorMessage} from 'formik';

export function Input(props) {
    const [field, meta] = useField(props);
    const isError = meta.touched && meta.error;
  return (
    <FormField>
        <InputSUR {...field} {...props} error={!!isError}/>
        <ErrorMessage name={props.name} component="div" className='error-text' />
    </FormField>
  )
}
