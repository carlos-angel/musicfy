import { FormField, Dropdown as DropdownSUR } from "semantic-ui-react";
import { useField, ErrorMessage } from "formik";

export function Dropdown(props) {
  const [field, meta] = useField(props);
  const isError = meta.touched && meta.error;
  return (
    <FormField>
      <DropdownSUR {...field} {...props} error={!!isError} />
      <ErrorMessage name={props.name} component="div" className="error-text" />
    </FormField>
  );
}
