import { useState, useEffect, useCallback } from "react";
import { Button, Image, FormField, FormGroup } from "semantic-ui-react";
import { useDropzone } from "react-dropzone";
import { toast } from "react-toastify";
import { map } from "lodash";
import { Formik, Form } from "formik";
import { Input, Dropdown } from "components/common/Form";
import { useArtists } from "hooks/useArtists.hook";
import { addAlbum } from "services/album";
import * as Yup from "yup";
import NoImage from "assets/png/no-image.png";
import "./styles.scss";

export const AlbumForm = ({ setShowModal }) => {
  const [albumImage, setAlbumImage] = useState(null);
  const [file, setFile] = useState(null);
  const [dataDropdown, setDataDropdown] = useState([]);

  const artists = useArtists();

  useEffect(() => {
    if (artists) {
      const data = map(artists, (artist) => ({
        key: artist.id,
        text: artist.name,
        value: artist.id,
      }));
      setDataDropdown(data);
    }
  }, [artists]);

  const onDrop = useCallback((acceptedFiles) => {
    const file = acceptedFiles[0];
    setFile(file);
    setAlbumImage(URL.createObjectURL(file));
  }, []);

  const { getRootProps, getInputProps } = useDropzone({
    accept: "image/jpg, image/png, image/jpeg",
    noKeyboard: true,
    onDrop,
  });

  return (
    <Formik
      initialValues={{ artist: "", name: "" }}
      validationSchema={Yup.object({
        artist: Yup.string().required("El artista es requerido"),
        name: Yup.string().required("El nombre es requerido"),
      })}
      onSubmit={async (values) => {
        if (!file) {
          toast.error("Debe seleccionar una imagen");
          return;
        }
        addAlbum({ ...values, banner: file });
        setShowModal(false);
      }}
    >
      {({ isSubmitting, setFieldValue, values, handleBlur }) => (
        <Form className="album-form">
          <FormGroup>
            <FormField className="album-avatar" width={5}>
              <div
                {...getRootProps()}
                className="avatar"
                style={{ backgroundImage: `url('${albumImage}')` }}
              />
              {!albumImage && <Image src={NoImage} />}
              <input {...getInputProps()} />
            </FormField>

            <Input placeholder="Nombre del album" type="text" name="name" />

            <Dropdown
              placeholder="El album pertenece a..."
              name="artist"
              fluid
              search
              selection
              lazyLoad
              options={dataDropdown}
              value={values.artist}
              onChange={(e, { value }) => setFieldValue("artist", value)}
              onBlur={() => handleBlur("artist")}
            />
          </FormGroup>
          <Button loading={isSubmitting} disabled={isSubmitting} type="submit">
            Crear album
          </Button>
        </Form>
      )}
    </Formik>
  );
};
