import React, { useState, useEffect } from "react";
import { Menu, Icon } from "semantic-ui-react";
import { Link, withRouter } from "react-router-dom";
import { BasicModal } from "components/Modal/Basic";
import { useModal } from "hooks/useModal";
import { ArtistForm } from "components/Artists/ArtistForm";
import { AlbumForm } from "components/Albums/AlbumForm";
import { SongForm } from "components/Songs/SongForm";
import { useAuth } from "hooks/useAuth.hook";
import "./styles.scss";

const MenuLeftComponent = ({ location }) => {
  const { isAdmin } = useAuth();
  const [activeMenu, setActiveMenu] = useState(location.pathname);

  const {
    properties: { show, title, content },
    setTitle,
    setShow,
    setContent,
  } = useModal();

  useEffect(() => {
    setActiveMenu(location.pathname);
  }, [location]);

  const handleMenu = (e, menu) => {
    setActiveMenu(menu.to);
  };

  const handleModal = (type) => {
    switch (type) {
      case "artist":
        setTitle("Nuevo Artista");
        setShow(true);
        setContent(<ArtistForm setShowModal={setShow} />);
        break;

      case "song":
        setTitle("Nueva Canción ");
        setShow(true);
        setContent(<SongForm setShowModal={setShow} />);
        break;
      case "album":
        setTitle("Nuevo Album");
        setShow(true);
        setContent(<AlbumForm setShowModal={setShow} />);
        break;
      default:
        setShow(false);
        break;
    }
  };

  return (
    <>
      <Menu className="menu-left" vertical>
        <div className="top">
          <Menu.Item
            as={Link}
            to="/"
            active={activeMenu === "/"}
            onClick={handleMenu}
          >
            <Icon name="home" /> Inicio
          </Menu.Item>
          <Menu.Item
            as={Link}
            to="/artists"
            active={activeMenu === "/artists"}
            onClick={handleMenu}
          >
            <Icon name="user" /> Artistas
          </Menu.Item>
          <Menu.Item
            as={Link}
            to="/albums"
            active={activeMenu === "/albums"}
            onClick={handleMenu}
          >
            <Icon name="music" /> Álbumes
          </Menu.Item>
        </div>
        {isAdmin && (
          <div className="footer">
            <Menu.Item onClick={() => handleModal("artist")}>
              <Icon name="plus square outline" /> Nuevo Artista
            </Menu.Item>
            <Menu.Item onClick={() => handleModal("album")}>
              <Icon name="plus square outline" /> Nuevo Album
            </Menu.Item>
            <Menu.Item onClick={() => handleModal("song")}>
              <Icon name="plus square outline" /> Nueva Canción
            </Menu.Item>
          </div>
        )}
      </Menu>
      <BasicModal setShow={setShow} show={show} title={title}>
        {content}
      </BasicModal>
    </>
  );
};

export const MenuLeft = withRouter(MenuLeftComponent);
