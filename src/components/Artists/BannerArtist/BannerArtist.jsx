import { useImage } from "hooks/useImage.hook";
import "./styles.scss";

export const BannerArtist = ({ artist }) => {
  const banner = useImage({ folder: "artist", imageName: artist.banner });
  return (
    <div
      className="banner-artist"
      style={{ backgroundImage: `url('${banner}')` }}
    >
      <div className="banner-artist__gradient" />
      <div className="banner-artist__info">
        <h4>Artista</h4>
        <h1>{artist?.name}</h1>
      </div>
    </div>
  );
};
