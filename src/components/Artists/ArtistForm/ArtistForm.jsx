import { useState, useCallback } from "react";
import { Button, Image, FormField } from "semantic-ui-react";
import { useDropzone } from "react-dropzone";
import { Formik, Form } from "formik";
import { Input } from "components/common/Form";
import { toast } from "react-toastify";
import { addArtist } from "services/artists";
import * as Yup from "yup";
import NotImage from "assets/png/no-image.png";
import "./styles.scss";

export const ArtistForm = ({ setShowModal }) => {
  const [banner, setBanner] = useState(null);
  const [file, setFile] = useState(null);

  const onDrop = useCallback((acceptedFile) => {
    const file = acceptedFile[0];
    setFile(file);
    setBanner(URL.createObjectURL(file));
  }, []);

  const { getRootProps, getInputProps } = useDropzone({
    accept: "image/jpeg, image/png, image/jpg",
    noKeyboard: true,
    onDrop,
  });

  return (
    <Formik
      initialValues={{ name: "" }}
      validationSchema={Yup.object({
        name: Yup.string().required(
          "El nombre del artista no puede estar vacío"
        ),
      })}
      onSubmit={async ({ name }) => {
        if (!file) {
          toast.error("Añade una imagen al artista");
        } else {
          const isAdded = await addArtist({ name, banner: file });
          if (isAdded) {
            setShowModal(false);
            setFile(null);
            setBanner(null);
          }
        }
      }}
    >
      {({ isSubmitting }) => (
        <Form className='add-artist-form'>
          <FormField className='artist-banner'>
            <div
              {...getRootProps()}
              className='banner'
              style={{ backgroundImage: `url('${banner}')` }}
            />
            {!banner && <Image src={NotImage} />}
            <input {...getInputProps()} />
          </FormField>
          <FormField className='artist-avatar'>
            <div
              className='avatar'
              style={{ backgroundImage: `url('${banner || NotImage}')` }}
            />
          </FormField>

          <Input placeholder='Nombre del artista' type='text' name='name' />

          <Button loading={isSubmitting} disabled={isSubmitting} type='submit'>
            Agregar
          </Button>
        </Form>
      )}
    </Formik>
  );
};
