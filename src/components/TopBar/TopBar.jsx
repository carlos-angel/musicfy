import { Icon, Image } from "semantic-ui-react";
import { Link, withRouter } from "react-router-dom";
import UserImage from "assets/png/user.png";
import { useAuth } from "hooks/useAuth.hook";
import "./styles.scss";

export const TopBarComponent = ({ history }) => {
  const { user, logout } = useAuth();
  const goBack = () => history.goBack();

  return (
    <div className="top-bar">
      <div className="top-bar__left">
        <Icon name="angle left" onClick={goBack} />
      </div>
      <div className="top-bar__right">
        <Link to="/settings">
          <Image src={user?.photoURL || UserImage} />
          {user?.displayName}
        </Link>
        <Icon name="power off" onClick={logout} />
      </div>
    </div>
  );
};

export const TopBar = withRouter(TopBarComponent);
