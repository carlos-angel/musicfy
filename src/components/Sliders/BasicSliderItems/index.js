import { map, size } from "lodash";
import Slider from "react-slick";
import { Link } from "react-router-dom";
import { useImage } from "hooks/useImage.hook";
import "./styles.scss";

export const BasicSliderItems = ({ title, data, folderImages, urlName }) => {
  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    centerMode: true,
    className: "basic-slider-items__list",
  };

  return size(data) < 5 ? null : (
    <div className="basic-slider-items">
      <h2>{title}</h2>
      <Slider {...settings}>
        {map(data, (artist) => (
          <Item
            key={artist.id}
            {...artist}
            folderImages={folderImages}
            urlName={urlName}
          />
        ))}
      </Slider>
    </div>
  );
};

function Item({ id, name, banner, folderImages, urlName }) {
  const image = useImage({ folder: folderImages, imageName: banner });
  return (
    <Link to={`/${urlName}/${id}`}>
      <div className="basic-slider-items__list-item">
        <div
          className="avatar"
          style={{ backgroundImage: `url('${image}')` }}
        />
        <h3>{name}</h3>
      </div>
    </Link>
  );
}
