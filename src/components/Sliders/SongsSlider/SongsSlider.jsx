import { Link } from "react-router-dom";
import { size, map } from "lodash";
import Slider from "react-slick";
import { Icon } from "semantic-ui-react";
import { useImage } from "hooks/useImage.hook";
import { useAlbum } from "hooks/useAlbum.hook";
import { usePlayer } from "hooks/usePlayer.hook";
import "./styles.scss";

export const SongsSlider = ({ title, data }) => {
  const { player } = usePlayer();

  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    centerMode: true,
    className: "songs-slider__list",
  };

  return size(data) < 5 ? (
    <div />
  ) : (
    <div className="songs-slider">
      <h2>{title}</h2>
      <Slider {...settings}>
        {map(data, (item) => (
          <Song key={item.id} {...item} player={player} />
        ))}
      </Slider>
    </div>
  );
};

function Song({ name, fileName, album, player }) {
  const albumData = useAlbum({ id: album });
  const banner = useImage({ folder: "album", imageName: albumData?.banner });

  const onPlay = async () => {
    await player({
      albumImage: banner,
      songName: name,
      songNameFile: fileName,
    });
  };

  return (
    <div className="songs-slider__list-song">
      <div
        className="avatar"
        style={{ backgroundImage: `url('${banner}')` }}
        onClick={onPlay}
      >
        <Icon name="play circle outline" />
      </div>
      <Link to={`/album/${album}`}>
        {" "}
        <h3>{name}</h3>
      </Link>
    </div>
  );
}
