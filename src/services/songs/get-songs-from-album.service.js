import {
  getFirestore,
  collection,
  where,
  getDocs,
  query,
} from "firebase/firestore";

export async function getSongsFromAlbum(albumId) {
  const db = getFirestore();
  const ref = collection(db, "songs");

  const q = query(ref, where("album", "==", albumId));

  const snapshot = await getDocs(q);
  const data = snapshot.docs.map((doc) => {
    return { ...doc.data(), id: doc.id };
  });

  return data || [];
}
