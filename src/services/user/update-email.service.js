import { getAuth } from "firebase/auth";
import { updateEmail as updateEmailFirebase } from "firebase/auth";
import { toast } from "react-toastify";
import { reauthenticate } from "services/auth";

export async function updateEmail({ email, password }) {
  try {
    const isReauthenticate = await reauthenticate({ password });
    if (isReauthenticate) {
      const auth = getAuth();
      await updateEmailFirebase(auth.currentUser, email);
      toast.success("Email actualizado");
      return true;
    } else {
      toast.error("La contraseña es incorrecta");
      return false;
    }
  } catch (error) {
    toast.error("Error al actualizar el email");
    return false;
  }
}
