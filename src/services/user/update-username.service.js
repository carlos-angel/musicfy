import { updateProfile } from "firebase/auth";

export async function updateUsername({ userFirebase, username }) {
   await updateProfile(userFirebase, { displayName: username });
}
