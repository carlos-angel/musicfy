export { getArtists } from "./artists.service";
export { getArtist, addArtist } from "./artist.service";
