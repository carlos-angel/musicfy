import {
  collection,
  doc,
  getDocs,
  getFirestore,
  getDoc,
} from "firebase/firestore";

export async function getAlbums() {
  const db = getFirestore();
  const ref = collection(db, "albums");

  const snapshot = await getDocs(ref);
  const data = snapshot.docs.map((doc) => {
    return { ...doc.data(), id: doc.id };
  });

  return data || [];
}

export async function getAlbum(id) {
  const db = getFirestore();
  const docRef = doc(db, "albums", id);

  const snapshot = await getDoc(docRef);
  const existsSnapshot = snapshot.exists();

  const data = existsSnapshot ? { id: snapshot.id, ...snapshot.data() } : false;

  return data;
}
