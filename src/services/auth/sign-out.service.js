import {getAuth} from 'firebase/auth';

export function signOut(){
    getAuth().signOut();
}