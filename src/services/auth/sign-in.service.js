import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { toast } from "react-toastify";
import { handleErrorFirebase } from "utils/errors-firebase.utility";
import { userVerified } from "./send-email-verification.service";

export function signIn({ email, password }) {
  const auth = getAuth();
  return signInWithEmailAndPassword(auth, email, password)
    .then(async (userCredential) => {
      if (userCredential.user.emailVerified) {
        return { error: false, user: userCredential.user };
      } else {
        await userVerified({ userFirebase: userCredential.user });
        toast.warning("Por favor, verifica tu email para poder iniciar sesión");
        return { error: true, user: null };
      }
    })
    .catch((error) => {
      handleErrorFirebase(error.code);
      return { error: true, user: null };
    });
}
