import { getStorage, ref, getDownloadURL } from "firebase/storage";

export async function getImage({ folder, imageName }) {
  const storage = getStorage();
  const imageRef = ref(storage, `${folder}/${imageName}`);
  const imageUrl = await getDownloadURL(imageRef);
  return imageUrl;
}
